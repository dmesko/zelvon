package main

import (
	"log"

	rpio "github.com/stianeikeland/go-rpio"
)

type fakePin struct {
}

func (*fakePin) Write(state rpio.State) {
	log.Println("Writing fake state", state)
}

func (*fakePin) Read() rpio.State {
	log.Println("Returning fake High state")
	return rpio.High
}

func (*fakePin) Output() {
	log.Println("Setting pin to fake Output")
}
