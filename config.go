package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/dmesko/utils"
)

// Config struct is app wide configuration structure

type Config struct {
	TimeOn struct {
		Hour   int
		Minute int
	}
	TimeOff struct {
		Hour   int
		Minute int
	}
	RelayGPIOPort int
	Debug         bool
	Fake          bool
}

// GetConfig loads config.json to app wide structure
func GetConfig(configHomeDir string) (c *Config, err error) {
	file, err := os.Open("config.json")
	if err != nil {
		log.Println("Config not found in working directory, trying user's home")
		cfgName, err2 := utils.GetHomeDirConfigFileName("config.json", configHomeDir)
		if err2 != nil {
			log.Println("Error obtaining home directory:", err2)
			return c, err2
		}
		var err3 error
		file, err3 = os.Open(cfgName)
		if err3 != nil {
			log.Printf("Config not found in ~/%s", configHomeDir)
			return c, err3
		}
	}
	decoder := json.NewDecoder(file)
	c = &Config{}
	err = decoder.Decode(c)
	if err != nil {
		log.Fatalln("Config decode error")
		return c, err
	}
	log.Println("Config has been loaded and decoded fine")
	return
}

// ProcessArgs process arguments - like splitting, or assigning default values
func ProcessArgs() *Config {
	flag.Usage = func() {
		fmt.Println("Usage:")
		flag.PrintDefaults()
	}
	debugFlag := flag.Bool("debug", true, "Debug to console")
	helpFlag := flag.Bool("help", false, "")
	flag.Parse() // Scan the arguments list

	if *helpFlag {
		flag.Usage()
		os.Exit(0)
	}

	showLog(*debugFlag) // show log asap (and debug config errors)

	cfg, err := GetConfig(".zelvon")
	if err != nil {
		log.Fatalf("Config not found in working directory nor home folder")
	}
	if *debugFlag || cfg.Debug {
		cfg.Debug = true
		showLog(true)
	}

	log.Println("Processed config:", cfg)
	return cfg
}

func showLog(show bool) {
	if show {
		log.SetOutput(os.Stdout)
	} else {
		log.SetOutput(ioutil.Discard)
	}
}
