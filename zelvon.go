package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	cfg := ProcessArgs()

	la := NewLamp(cfg)
	defer la.ReleaseLamp()

	mt := time.NewTicker(1 * time.Minute)
	go evaluateState(mt, la, cfg)

	done := make(chan bool, 1)
	go sigHandler(done)
	<-done
	fmt.Println("Exiting after signal")
}

func createBoundaryTime(hour, minute int) time.Time {
	now := time.Now()
	return time.Date(now.Year(), now.Month(), now.Day(), hour, minute, 0, 0, now.Location())
}

func sigHandler(done chan<- bool) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigs
	fmt.Println()
	fmt.Println(sig)
	done <- true
}

func evaluateState(mt *time.Ticker, la *lamp, cfg *Config) {
	for t := range mt.C {
		startTime := createBoundaryTime(cfg.TimeOn.Hour, cfg.TimeOn.Minute)
		endTime := createBoundaryTime(cfg.TimeOff.Hour, cfg.TimeOff.Minute)
		if t.Before(startTime) || t.After(endTime) {
			la.setItOff()
			continue
		}
		if t.After(startTime) && t.Before(endTime) {
			la.setItOn()
			continue
		}
	}
}
