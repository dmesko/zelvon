package main

import (
	"fmt"
	"log"
	"os"

	rpio "github.com/stianeikeland/go-rpio"
)

type CustomPIN interface {
	Write(state rpio.State)
	Read() rpio.State
	Output()
}

type lamp struct {
	GPIOPin CustomPIN
}

func NewLamp(cfg *Config) *lamp {
	var pin CustomPIN
	if !cfg.Fake {
		if err := rpio.Open(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		pin = rpio.Pin(cfg.RelayGPIOPort)
		pin.Output()
	} else {
		pin = &fakePin{}
	}
	return &lamp{GPIOPin: pin}
}

func (l *lamp) ReleaseLamp() {
	rpio.Close()
}

func (l *lamp) isItOn() bool {
	state := l.GPIOPin.Read()
	if state == rpio.High {
		return true
	}
	return false
}

func (l *lamp) setItOn() {
	log.Println("Lamp ON")
	l.GPIOPin.Write(rpio.High)
}

func (l *lamp) setItOff() {
	log.Println("Lamp OFF")
	l.GPIOPin.Write(rpio.Low)
}
